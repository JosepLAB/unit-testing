/** Basic arithmetic ope */
const mylib = {
    /** Multiline arrow function */
    add: (a, b) => {
        const sum = a + b;
        return sum;
    },
    substract: (a, b) => {
        return a - b;
    },
    
    divide: function(dividend, divisor) {
        if(divisor == 0) {
            throw new Error("Cannot divide by 0!");
        }
        return dividend / divisor;
    },
    /** Regular function */
    multiply: function(a, b) {
        return a * b;
    }
};

module.exports = mylib;