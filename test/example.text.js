const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("Our first unit test", () => {
    before(() => {
        //init
        //create obj...
        console.log("Initializing tests");
    });
    it("Can add 1 and 2 together", () => {
        expect(mylib.add(1,2)).equal(3, "1 + 2 in not 3 for some reason?");
    });
    it("Can substract 1 from 2", () => {
        expect(mylib.substract(2,1)).equal(1, "2 - 1 in not 1 for some reason?");
    });
    it("Can divide 8 by 2", () => {
        expect(mylib.divide(8,2)).equal(4, "8 / 2 in not 4 for some reason?");
    });
    it("Cannot divide by 0", () => {
        expect(() => mylib.divide(8,0)).to.throw('Cannot divide by 0!');
    });
    it("Can multiply 8 by 2", () => {
        expect(mylib.multiply(8,2)).equal(16, "8 * 2 in not 16 for some reason?");
    });
    after(() => {
        //cleanup
        console.log("Testing completed!");
    });
});